# Operator SDK Images

This repository contains operator-sdk modified source code images that are based on fedora images instead of rhel, the upstream work can be found here: https://github.com/operator-framework/operator-sdk/blob/master/images/

Only ansible is currently supported.

Images are available with as both latest tag and the current stable release of fedora.

Availale images

* quay.io/fedora-kube-sig/ansible-operator:latest
* quay.io/fedora-kube-sig/ansible-operator:f34

## License

Apache 2.0 (see license file)
