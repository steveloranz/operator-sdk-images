# Ansible Images

This folder contains Dockerfiles for the ansible based operator.

Those are modified files from the original upsyream operator-sdk repository  which can be found at https://github.com/operator-framework/operator-sdk/blob/master/images/ansible-operator

The operator-sdk image depends on the "base" image, which contains the base components needed for the ansible operator.

A `Makefile` is provided for convenience for each folder.
